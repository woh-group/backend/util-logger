package util

import (
	"fmt"
	"net/http"
	"time"
)

// Operacion
const (
	ADD    = "ADD"
	UPDATE = "UPDATE"
	DELETE = "DELETE"
)

// Tipo
const (
	REQUEST  = "REQUEST"
	RESPONSE = "RESPONSE"
)

// Nivel
const (
	INFO  = "INFO"
	ERROR = "ERROR"
)

// Headers
const (
	RequestID = "Request-Id"
	AppID     = "App-Id"
)

// Archivos Loggers
const (
	infoFile = "info.log"
)

// LoggerRequest loguea un request
type LoggerRequest struct {
	Fecha      time.Time   `bson:"fecha" json:"fecha"`
	Tipo       string      `bson:"tipo" json:"tipo"`
	Level      string      `bson:"level" json:"level"`
	RequestID  string      `bson:"requestId" json:"requestId"`
	AppID      string      `bson:"appId" json:"appId"`
	Username   string      `bson:"username" json:"username"`
	IPCliente  string      `bson:"ipCliente" json:"ipCliente"`
	IPServidor string      `bson:"ipServidor" json:"ipServidor"`
	API        string      `bson:"api" json:"api"`
	Metodo     string      `bson:"metodo" json:"metodo"`
	URI        string      `bson:"uri" json:"uri"`
	Body       interface{} `bson:"body" json:"body"`
}

// LoggerResponse loguea un response
type LoggerResponse struct {
	Fecha      time.Time   `bson:"fecha" json:"fecha"`
	Tipo       string      `bson:"tipo" json:"tipo"`
	Level      string      `bson:"level" json:"level"`
	RequestID  string      `bson:"requestId" json:"requestId"`
	AppID      string      `bson:"appId" json:"appId"`
	Username   string      `bson:"username" json:"username"`
	IPCliente  string      `bson:"ipCliente" json:"ipCliente"`
	IPServidor string      `bson:"ipServidor" json:"ipServidor"`
	API        string      `bson:"api" json:"api"`
	Metodo     string      `bson:"metodo" json:"metodo"`
	URI        string      `bson:"uri" json:"uri"`
	Data       interface{} `bson:"data" json:"data"`
	StatusCode int         `bson:"statusCode" json:"statusCode"`
	Message    string      `bson:"message" json:"message"`
}

// LoggerGeneral loguea un log general
type LoggerGeneral struct {
	Fecha      time.Time `bson:"fecha" json:"fecha"`
	Level      string    `bson:"level" json:"level"`
	RequestID  string    `bson:"requestId" json:"requestId"`
	AppID      string    `bson:"appId" json:"appId"`
	Username   string    `bson:"username" json:"username"`
	IPCliente  string    `bson:"ipCliente" json:"ipCliente"`
	IPServidor string    `bson:"ipServidor" json:"ipServidor"`
	API        string    `bson:"api" json:"api"`
	Method     string    `bson:"method" json:"method"`
	URI        string    `bson:"uri" json:"uri"`
	StatusCode int       `bson:"statusCode" json:"statusCode"`
	Message    string    `bson:"message" json:"message"`
}

// PrintLog escribe un log generico
func PrintLog(collectionName string, level string, ipCliente string, ipServidor string, method string, headers http.Header, username string, uri string, statusCode int, message string) {
	var loggerObj LoggerGeneral

	loggerObj.Level = level
	loggerObj.Fecha = time.Now()
	loggerObj.RequestID = headers.Get(RequestID)
	loggerObj.AppID = headers.Get(AppID)
	loggerObj.Username = username
	loggerObj.IPCliente = ipCliente
	loggerObj.IPServidor = ipServidor
	loggerObj.API = collectionName
	loggerObj.Method = method
	loggerObj.URI = uri
	loggerObj.StatusCode = statusCode
	loggerObj.Message = message

	Print(collectionName, loggerObj)
}

// PrintRequest escribe el request en un log
func PrintRequest(collectionName string, ipCliente, ipServidor string, headers http.Header, username string, api string, metodo string, uri string, body interface{}) {
	var loggerObj LoggerRequest

	loggerObj.Fecha = time.Now()
	loggerObj.Tipo = REQUEST
	loggerObj.Level = INFO
	loggerObj.RequestID = headers.Get(RequestID)
	loggerObj.AppID = headers.Get(AppID)
	loggerObj.Username = username
	loggerObj.IPCliente = ipCliente
	loggerObj.IPServidor = ipServidor
	loggerObj.API = api
	loggerObj.Metodo = metodo
	loggerObj.URI = uri
	loggerObj.Body = body

	Print(collectionName, loggerObj)
}

// PrintResponse escribe el response en un log
func PrintResponse(collectionName string, ipCliente, ipServidor string, level string, headers http.Header, username string, api string, metodo string, uri string, data interface{}, statusCode int, message string) {
	var loggerObj LoggerResponse

	loggerObj.Fecha = time.Now()
	loggerObj.Tipo = RESPONSE
	loggerObj.Level = level
	loggerObj.RequestID = headers.Get(RequestID)
	loggerObj.AppID = headers.Get(AppID)
	loggerObj.Username = username
	loggerObj.IPCliente = ipCliente
	loggerObj.IPServidor = ipServidor
	loggerObj.API = api
	loggerObj.Metodo = metodo
	loggerObj.URI = uri
	loggerObj.Data = data
	loggerObj.StatusCode = statusCode
	loggerObj.Message = message

	Print(collectionName, loggerObj)
}

// Print escribe un log
func Print(collectionName string, request interface{}) {

	// file, err := os.OpenFile(infoFile, os.O_CREATE|os.O_APPEND, 0644)
	// if err != nil {
	// 	log.Fatal(err)
	// }

	// defer file.Close()

	// log.SetOutput(file)
	// log.Print("logger ->", request)

	var loggerRepository = LoggerRepository{}
	if err := loggerRepository.Create(collectionName, request); err != nil {
		fmt.Println("ERROR LOGGER ->", err)
	}
	// fmt.Println("logger ->", request)
}
